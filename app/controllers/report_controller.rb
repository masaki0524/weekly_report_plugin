class ReportController < ApplicationController
  unloadable
  before_filter :find_project, :authorize, :only => :index

  # プラグインのトップページ
  def index
    #メール用データの作成(途中)
    @mail_text = ""
    @target_users.each do |user|
       str = ""
#      str = user.to_s + "%0D%0A"
      str = user.to_s.encode("Shift_JIS") + "%0D%0A"
      
      @target_projects.each do |project|
        project.issues.each do |issue|

#          str << project.to_s + "%0D%0A"

     #          if user == issue.assigned_to && issue.status.id != 5 && issue.status.id != 6 && issue.start_date < Time.now + 30.days then
     #       #   if is_disp_issue(user, issue) && issue.parent_id.blank?
     #            str << issue.id + " : " + issue.subject + "%0D%0A"
     ##            str << "%20%20#" + issue.id.to_s + " : " + issue.subject + " (" + issue.start_date.strftime("%m/%d").to_s + "-" + issue.due_date.strftime("%m/%d").to_s  + ") ["+ issue.status.name + "]" + "%0D%0A"
     #            str << "%20%20#" + issue.id.to_s + " : " + " (" + issue.start_date.strftime("%m/%d").to_s + "-" + issue.due_date.strftime("%m/%d").to_s  + ") ["+ issue.status.name + "]" + "%0D%0A"
     #            issue.journals.each do |j|
     #              if j.created_on > Time.now - 7.days && j.notes.present? then # 1週間のコメント
     #  #            str << j.created_on.to_s.split[0] + " " + j.notes.delete("<").delete(">") + "\n"
     ##                str << "%20%20%20%20" + j.created_on.strftime("[%m/%d]") + "%0D%0A%20%20%20%20" + j.notes.delete("<").delete(">").to_s.gsub(/(\r\n|\r|\n)/, "%0D%0A%20%20%20%20") + "%0D%0A"
     #              end
     #            end # issue.journals.each.do
     #          end # if user == issue.assigned_to
        end # @project.issues.each do
      end
         #      @mail_text << str.encode("Shift_JIS")
         #      @mail_text << str.encode("UTF-8")
        @mail_text << str
    end #@target_users.each do
      @mail_title = "Project Weekly Report"
  end

  private
  # プロジェクトの選択
  def find_project
    @all_projects = Project.all
    @project = Project.find(params[:project_id])
    @project_id = @project.id
    @target_projects = []
    @target_users = []
    @users = []
    
    @all_projects.each do |prj|
      if (prj.id == @project_id || prj.parent_id == @project_id) && prj.status != 5
        @target_projects << prj
        prj.users.each do |user|
          @target_users << user
          @users << user
        end
      end
    end
    
    @target_users.uniq!
    @target_users.sort!

  rescue ActiveRecord::RecordNotFound
    render_404
  end
end
