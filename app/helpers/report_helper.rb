# -*- encoding: UTF-8 -*-
module ReportHelper
  def period
   start_date = Time.now - 10.days
   end_date = Time.now + 10.days
   
   start_date.strftime("%Y/%m/%d") + " - " + end_date.strftime("%Y/%m/%d")
  end
  
  def is_disp_user(user, target_projects)
    target_projects.each do |project|
      project.issues.each do |issue|
        if is_disp_issue(user, issue)
          return true
        end
      end
    end
    return false
  end

  def is_disp_project(project, current_project)
    project.id == current_project || project.parent_id == current_project
  end

  def is_disp_issue(user, issue)
    user.name == issue.assigned_to.to_s && issue.status.id != 5 && issue.status.id != 6 && issue.start_date < Time.now + 10.days
  end
  
  def is_parent(user, issue, project)
    #孫までは対応
    project.issues.each do |sub_issue|
      if issue.id == sub_issue.parent_id && is_disp_issue(user, sub_issue)
        return true
      end
      
      if issue.id == sub_issue.parent_id
        project.issues.each do |sub_sub_issue|
          if sub_issue.id == sub_sub_issue.parent_id && is_disp_issue(user, sub_sub_issue)
            return true
          end
        end
      end
    end
    return false
  end
  
  def is_disp_journal journal
    journal.notes.present? && journal.created_on > Time.now - 10.days #journalが存在し、直近10日間に更新されたもの
  end
  
  def issues_sorted(issues)
    sorted = issues.sort{|aa, bb| 
      aa.due_date <=> bb.due_date
    }
#         prj.issues = array
##         prj.issues.clear
#         prj.issues << array
#         prj.issues.slice!(0,array.size)
    return sorted
  end
  
  def status_word status
#    if status.id == 1
#      "未"
#    elsif status.id == 2
#      "中"
#    elsif status.id == 3
#      "済"
#    elsif status.id == 4
#      "済"
#    elsif status.id == 5
#      "済"
#    elsif status.id == 6
#      "済"
#    end
  end
  
  def start_date_color(issue)
    if issue.status.id == 1 && issue.start_date < Time.now #Newで開始日が過ぎている
      "red"
    else
      "black"
    end
  end
  
  def due_date_color(issue)
    if issue.status.id == 2 && issue.due_date < Time.now #In Progressで終了日が過ぎている
      "red"
    else
      "black"
    end
  end
end
