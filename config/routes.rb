# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  resources :projects do
    get 'report'      => 'report#index'
    get 'report/list' => 'report#list'
  end
end