Redmine::Plugin.register :weekly_report do
  name 'Weekly Report Plugin'
  author 'nakasuji'
  description 'This is a weekly report plugin'
  version '0.0.1'
  url 'https://bitbucket.org/masaki0524/weekly_report_plugin/src/master/'
  author_url 'https://bitbucket.org/masaki0524/weekly_report_plugin/src/master/'
  
  # 引数はplugin名
  project_module :weekly_report do
    # 引数は権限名, controllerとactionからなるhash
    permission :view_report, :report => [:index]
  end
  # 引数はメニューのタイプ, メニュー名, メニュークリック時に呼び出されるaction, オプション
  menu :project_menu, :report, { :controller => 'report', :action => 'index' }, :caption => :weekly_report, :param => :project_id
# menu :project_menu, :weekly_report, { :controller => 'weekly_report', :action => 'index' }, :caption => :weekly_report, :last => true, :param => :project_id

end
